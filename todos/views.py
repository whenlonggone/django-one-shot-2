from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import ListForm, AddItemForm


# Create your views here.
def show_TodoList(request):
    toDoList = TodoList.objects.all()
    context = {"TodoList": toDoList}
    return render(request, "toDoList/list.html", context)


def show_TodoItem(request, id):
    toDoList = get_object_or_404(TodoList, id=id)
    context = {"TodoList": toDoList}
    return render(request, "toDoList/details.html", context)


def create_new_list(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
            new_list = form.save(False)
            new_list.save()
            return redirect("todo_list_detail", id=new_list.id)
    else:
        form = ListForm()
    context = {
        "new_list_form": form,
    }
    return render(request, "toDoList/create_list.html", context)


def update_list_name(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = ListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = ListForm(instance=list)
    context = {
        "edit_form": form,
    }
    return render(request, "toDoList/edit_list.html", context)


def update_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = AddItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", item.list.id)
    else:
        form = AddItemForm(instance=item)
    context = {
        "update_item_form": form,
    }
    return render(request, "toDoList/update_item.html", context)


def delete_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")
    return render(request, "toDoList/delete_list.html")


def create_item(request):
    if request.method == "POST":
        form = AddItemForm(request.POST)
        if form.is_valid():
            new_item = form.save(False)
            new_item.save()
            return redirect("todo_list_detail", new_item.list.id)
    else:
        form = AddItemForm()
    context = {
        "new_item_form": form,
    }
    return render(request, "toDoList/create_item.html", context)
